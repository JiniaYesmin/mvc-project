﻿namespace C_Sharp_MVC.Models
{
    public class StudentModel
    {
        public StudentModel(string name, string id, string program)
        {
            studentName = name;
            studentId = id;
            Program = program;
        }
   
        public string studentName { get; set; }
        public string studentId { get; set; }
        public string Program { get; set; }
    }
}
