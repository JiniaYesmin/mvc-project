﻿using C_Sharp_MVC.Models;
using Microsoft.AspNetCore.Mvc;

using C_Sharp_MVC.Service;
namespace C_Sharp_MVC.Controllers
{
    public class StudentController : Controller
    {
        public IActionResult StudentView()
        {
            var student = new service();
            List<StudentModel> list1 = new List<StudentModel>();
            list1 = student.StudentData();
            return View(list1);
        }
    }
}
