﻿using C_Sharp_MVC.Models;

namespace C_Sharp_MVC.Service
{
    public class service
    {
        public List<StudentModel> StudentData()
        {
            List<StudentModel> list = new List<StudentModel>();
            list.Add(new StudentModel("Jinia", "18203050", "BCSE"));
            list.Add(new StudentModel("Tarikul", "16303050", "BCSE"));
            list.Add(new StudentModel("Oishi", "19103107", "BSN"));
            list.Add(new StudentModel("Tanzila", "18203094", "BSEEE"));
            list.Add(new StudentModel("Mayesha", "17103175", "BSME"));
            list.Add(new StudentModel("Rafti", "16207021", "BBA"));
            list.Add(new StudentModel("Disha", "19206009", "BAE"));
            list = list.OrderBy(x => x.studentId).ToList();
            return list;
        }
    }
}
